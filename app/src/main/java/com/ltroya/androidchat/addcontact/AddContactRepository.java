package com.ltroya.androidchat.addcontact;

public interface AddContactRepository {
    void addContact(String email);
}
