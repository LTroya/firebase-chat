package com.ltroya.androidchat.addcontact;

public interface AddContactInteractor {
    void execute(String email);
}
