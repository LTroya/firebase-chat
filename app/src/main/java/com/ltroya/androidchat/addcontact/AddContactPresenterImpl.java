package com.ltroya.androidchat.addcontact;

import com.ltroya.androidchat.addcontact.AddContactPresenter;
import com.ltroya.androidchat.addcontact.events.AddContactEvent;
import com.ltroya.androidchat.addcontact.ui.AddContactView;
import com.ltroya.androidchat.lib.EventBus;
import com.ltroya.androidchat.lib.GreenRobotEventBus;

import org.greenrobot.eventbus.Subscribe;

public class AddContactPresenterImpl implements AddContactPresenter {
    private EventBus evenBus;
    private AddContactView view;
    private AddContactInteractor interactor;

    public AddContactPresenterImpl(AddContactView view) {
        this.view = view;
        this.evenBus = GreenRobotEventBus.getInstance();
        this.interactor = new AddContactInteractorImpl();
    }

    @Override
    public void onShow() {
        evenBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        evenBus.unregister(this);
    }

    @Override
    public void addContact(String email) {
        if (view != null) {
            view.hideInput();
            view.showProgress();
        }
        interactor.execute(email);
    }

    @Override
    @Subscribe
    public void onEventMainThread(AddContactEvent event) {
        if (view != null) {
            view.hideProgress();
            view.showInput();

            if (event.isError()) {
                view.contactNotAdded();
            } else {
                view.contactAdded();
            }
        }
    }
}
