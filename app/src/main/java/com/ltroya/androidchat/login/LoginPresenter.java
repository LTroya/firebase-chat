package com.ltroya.androidchat.login;

import com.ltroya.androidchat.login.events.LoginEvent;

public interface LoginPresenter {
    void onCreate();
    void onDestroy();
    void checkForAuthenticatedUser();
    void validateLogin(String email, String password);
    void registerNewuser(String email, String password);
    void onEventMainThread(LoginEvent event);
}
