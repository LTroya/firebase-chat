package com.ltroya.androidchat.lib;

import android.widget.ImageView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by LTroya on 6/12/16.
 */
public interface ImageLoader {
    void load(ImageView imgAvatar, String s);
}
