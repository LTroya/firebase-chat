package com.ltroya.androidchat.contactlist.ui.adapters;

import com.ltroya.androidchat.entities.User;

public interface OnItemClickListener {
    void onItemClick(User user);
    void onItemLongClick(User user);
}
