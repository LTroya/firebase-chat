package com.ltroya.androidchat.contactlist.ui;

import com.ltroya.androidchat.entities.User;

public interface ContactListView {
    void onContactAdded(User user);
    void onContactChanged(User user);
    void onContactRemoved(User user);
}
