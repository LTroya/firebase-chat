package com.ltroya.androidchat.contactlist;

public interface ContactListRepository {
    void signOff();
    String getCurrentUserEmail();
    void removeContact(String email);
    void destroyListener();
    void subscribeToContactListEvent();
    void unsubscribeToContactListEvent();
    void changeConnectionStatus(boolean online);
}
