package com.ltroya.androidchat.chat.events;

import com.ltroya.androidchat.entities.ChatMessage;

public class ChatEvent {
    private ChatMessage message;

    public ChatMessage getMessage() {
        return message;
    }

    public void setMessage(ChatMessage message) {
        this.message = message;
    }
}
