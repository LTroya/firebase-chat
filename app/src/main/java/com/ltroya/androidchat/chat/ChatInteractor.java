package com.ltroya.androidchat.chat;

/**
 * Created by LTroya on 6/13/16.
 */
public interface ChatInteractor {
    void sendMessage(String msg);
    void setRecipient(String recipient);

    void subscribe();
    void unsubscribe();
    void destroyListener();
}
