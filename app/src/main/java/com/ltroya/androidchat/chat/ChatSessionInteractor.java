package com.ltroya.androidchat.chat;

public interface ChatSessionInteractor {
    void changeConnectionStatus(boolean online);
}
