package com.ltroya.androidchat.chat.ui;

import com.ltroya.androidchat.entities.ChatMessage;

public interface ChatView {
    void onMessageReceived(ChatMessage msg);
}
